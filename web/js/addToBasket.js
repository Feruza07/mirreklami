$(document).ready(function () {
    $('.add').on('click', function () {
        var data = $('form').serialize();
        $.ajax({
            url: $('form').attr('action'),
            type: 'POST',
            data: data,
            success: function (res) {
                $('#basketItems').html(res);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
})