<?php

namespace app\controllers;

use app\models\Basket;
use Yii;

class ProductController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if(Yii::$app->request->isAjax) {
            $count = 0;
            $request = Yii::$app->request->post();
            $sessionId = Yii::$app->session->getId();
            Basket::deleteAll(['session_id' => $sessionId]);
            unset($request['_csrf']);
            foreach($request as $className => $groupProduct) {
                foreach ($groupProduct as $productId) {
                    $basket = new Basket();
                    $basket->session_id = $sessionId;
                    $basket->type = $className;
                    $basket->product_id = $productId;
                    $basket->save();
                }
                $count += count($groupProduct);
            }
            return $count;
        }
        return $this->render('index');
    }

}
