<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "basket".
 *
 * @property int $id
 * @property string $type
 * @property int $product_id
 * @property string $session_id
 */
class Basket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'basket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['type', 'session_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'product_id' => 'Product ID',
            'session_id' => 'Session ID',
        ];
    }

    public function getTransport(){
        return $this->hasOne(Transport::className(), ['transport_id' => 'product_id']);
    }

    public function getMetro(){
        return $this->hasOne(Metro::className(), ['metro_id' => 'product_id']);
    }

    public function getBillboard(){
        return $this->hasOne(Billboard::className(), ['billboard_id' => 'product_id']);
    }

}
