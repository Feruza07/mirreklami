<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\widgets\ProductListWidget;

?>
<?php $this->registerJsFile('/js/addToBasket.js', ['depends' => [\yii\web\JqueryAsset::className()]])?>

<div class="text-center"><a href="<?=Url::to(['/basket/index'])?>"><span class="badge">basket <span id="basketItems">0</span></span></a></div>
<?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-4">
            <h3>Billboards</h3>
            <?=ProductListWidget::widget(['modelClass' => 'Billboard', 'id' => 'billboard_id', 'name' => 'billboard_name'])?>
        </div>
        <div class="col-md-4">
            <h3>Transport</h3>
            <?=ProductListWidget::widget(['modelClass' => 'Transport', 'id' => 'transport_id', 'name' => 'transport_name'])?>
        </div>
        <div class="col-md-4">
            <h3>Metro</h3>
            <?=ProductListWidget::widget(['modelClass' => 'Metro', 'id' => 'metro_id', 'name' => 'metro_name'])?>
        </div>
    </div>
<?php ActiveForm::end()?>


