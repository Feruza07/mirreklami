<?php

use app\widgets\BasketWidget;

?>

<div>
    <?=BasketWidget::widget(['modelClass' => 'Transport', 'attrName' => 'transport_name'])?>
</div>
<div>
    <?=BasketWidget::widget(['modelClass' => 'Metro', 'attrName' => 'metro_name'])?>
</div>
<div>
    <?=BasketWidget::widget(['modelClass' => 'Billboard', 'attrName' => 'billboard_name'])?>
</div>

