<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transport}}`.
 */
class m190821_185013_create_transport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transport}}', [
            'transport_id' => $this->primaryKey(),
            'transport_name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transport}}');
    }
}
