<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%billboard}}`.
 */
class m190821_184932_create_billboard_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%billboard}}', [
            'billboard_id' => $this->primaryKey(),
            'billboard_name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%billboard}}');
    }
}
