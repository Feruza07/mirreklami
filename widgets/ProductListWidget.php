<?php

namespace app\widgets;

use yii\base\Widget;

class ProductListWidget extends Widget
{

    public $id;
    public $name;
    public $modelClass;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $modelName = 'app\models\\'.$this->modelClass;
        $products = $modelName::find()->select(['id' => $this->id, 'name' => $this->name])->asArray()->all();

        return $this->render('product', ['products' => $products, 'modelClass' => $this->modelClass]);
    }

}