<div class="list-group">
    <?php foreach($products as $key => $product): ?>
        <div class="list-group-item">
            <label for="<?=$key.'checkbox'?>"><?=$product['name']?></label>
            <input type="checkbox" value="<?=$product['id']?>" id="<?=$key.'checkbox'?>"  name="<?=$modelClass?>[]" class="add">
        </div>
    <?php endforeach; ?>
</div>