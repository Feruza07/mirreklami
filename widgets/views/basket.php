<?php $relation = lcfirst($title)?>
<div class="list-group">
    <h3><?=$title?>&nbsp;<span class="badge"><?=count($products)?></span></h3>
    <?php foreach($products as $key => $product): ?>
        <div class="list-group-item">
            <?=$product->$relation->$name?>
        </div>
    <?php endforeach; ?>
</div>