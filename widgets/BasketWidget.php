<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Basket;

class BasketWidget extends Widget
{
    public $attrName;
    public $modelClass;


    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $sessionId = Yii::$app->session->getId();
        $products = Basket::find()->where([
            'type' => $this->modelClass,
            'session_id' =>  $sessionId
        ])->all();

        return $this->render('basket', ['products' => $products, 'name' => $this->attrName, 'title' => $this->modelClass]);
    }

}